import Vue from "vue";
import Vuex from "vuex";
import infoService from "@/core/service/info.service";

Vue.use(Vuex);
const info = {
    namespaced: true,
    strict: true,
    state: {
        totalOfWorld: [],
        listCountry: [],
        totalInfection: [],
        isloadTotal: true,
        isloadTable: true,
    },

    mutations: {
        GET_TOTAL_OF_WORLD(state, payload) {
            state.totalOfWorld = payload;
            console.log(state.totalOfWorld);
        },
        GET_ALL_COUNTRIES(state, payload) {
            state.listCountry = payload;
        },
        GET_TOTAL_WITH_COUNTRY(state, payload) {
            state.totalInfection = payload;
        },
    },
    actions: {
        //Get disease information worldwide
        async getInfoOfTheWorld(vuexContext) {
            const res = await infoService.getInfoOfTheWorld("");
            if (res.status === 200) {
                vuexContext.state["isloadTotal"] = false;
                vuexContext.commit("GET_TOTAL_OF_WORLD", res.data);
            } else {
                vuexContext.state["isloadTotal"] = false;
                return res;
            }
        },

        //Get info All country for select box
        async getAllCountries(vuexContext) {
            const res = await infoService.getCountries();
            if (res.status === 200) {
                vuexContext.commit("GET_ALL_COUNTRIES", res.data);
            } else {
                return res;
            }
        },

        //Get disease information in selected country
        async getTotalInfection(vuexContext, countryName) {
            const res = await infoService.diseaseCountry(countryName);
            if (res.status === 200) {
                vuexContext.state["isloadTable"] = false;
                vuexContext.commit("GET_TOTAL_WITH_COUNTRY", res.data);
            } else {
                vuexContext.state["isloadTable"] = false;
                return res;
            }
        },
    },
    getters: {
        value: (state) => {
            return state.value;
        },
    },
};
export default info;