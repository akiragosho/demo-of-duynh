//common function call api GET/POST/PUT/DELETE 
import axios from 'axios';
import CONTANT from '@/core/contant';
import qs from 'qs';

// const headers = {
//   token: funcMemory.getCookie('token'),
//   email: funcMemory.getCookie('email'),
//   employeeCode: funcMemory.getCookie('employeeCode'),
//   Pragma: 'no-cache'
// }
// const headerImport = headers
// headerImport['Content-Type'] = 'multipart/form-data'
const funcApi = {
  
  post(url, data) {
    let dataTmp = "";
    if(qs.stringify(data).indexOf("%40") > 0) {
      dataTmp = qs.stringify(data).replace("%40", "@")
    }
    console.log(dataTmp);
    return axios({
      method: 'post',
      url: CONTANT.url + url + '?' + dataTmp
      // headers: headers
    });
  },

  put(url, data) {
    return axios({
      method: 'put',
      url: CONTANT.url + url,
      data: qs.stringify(data),
      // headers: headers
    });
  },

  get(url, data){
    return axios({
      method: 'get',
      url: CONTANT.url + url +  data,
      // headers: headers
    });
  },

}
export default funcApi
