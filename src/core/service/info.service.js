import funcApi from "@/core/api";

const infoService = {};
//Get info of the world

infoService.getInfoOfTheWorld = (data) => {
    return funcApi.get("world/total", data);
};

//Get all Country
infoService.getCountries = (data) => {
    return funcApi.get("countries?", data);
};

//Get info disease situation of the country
//Data =  country name
infoService.diseaseCountry = (data) => {
    console.log("data ", data);
    return funcApi.get("total/country/", data);
};

export default infoService;